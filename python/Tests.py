from tkinter import *
import sqlite3

conn = sqlite3.connect('Tests.db')
c = conn.cursor()

sql = 'CREATE TABLE IF NOT EXISTS TestInfo (id INTEGER NOT NULL, name TEXT, kolq INTEGER, sysmark TEXT, PRIMARY KEY(id))'
c.execute(sql)


def create_test():
    def next_step():
        numq = 0
        test_name = textbox_name.get()
        questions_number = textbox_questions_number.get()
        rate_system = str(check_test_type.get())

        sql = 'INSERT INTO TestInfo (name, kolq, sysmark) values (?, ?, ?)'
        c.execute(sql, (test_name, questions_number, rate_system,))
        print(c.execute("SELECT * FROM TestInfo").fetchall())
        conn.commit()

        c.execute('SELECT * FROM TestInfo WHERE name = ?', (test_name,))
        results = c.fetchall()
        for result in results:
            testi = result[0]

        window_create.destroy()
        window_fill_test = Tk()
        window_fill_test.title("Заполнение теста")
        window_fill_test.geometry("640x240")

        frame1 = Frame(window_fill_test, bd=5)
        frame2 = Frame(window_fill_test, bd=5)

        lbl = Label(window_fill_test, text="Заполнение теста:")
        lbl.grid(row=0, column=1, sticky=W)
        lbl = Label(window_fill_test, text="Тип ответов:")
        lbl.grid(row=1, column=0, sticky=W)

        def input_answers():
            def next_question_1():
                sql = 'create table if not exists TestText (id INTEGER, testid INTEGER, qtype TEXT, question TEXT, answer TEXT, varanswer TEXT)'
                c.execute(sql)
                conn.commit()
                c.execute("SELECT * FROM TestText WHERE id = ?", (testi,))
                result = c.fetchall()
                for chk in result:
                    chkn = chk[2]
                if result == [] or chkn == 3:
                    numq = 1
                    question = textbox_question.get()
                    answer = textbox_answer.get()
                    va = '---'
                    qt = '1'
                    c.execute("INSERT INTO TestText (id, testid, qtype, question, answer, varanswer) VALUES (?,?,?,?,?,?)", (numq, testi, qt, question, answer, va,))
                    conn.commit()
                    c.execute("SELECT * FROM TestText")
                    print(c.fetchall())
                else:
                    numq = 0
                    c.execute('SELECT * FROM TestInfo WHERE id = ?', (testi,))
                    results = c.fetchall()
                    for result in results:
                        kq = int(result[2])
                    c.execute('SELECT * FROM TestText WHERE testid = ?', (testi,))
                    results = c.fetchall()
                    for result in results:
                        numq = int(result[0])
                    if numq < kq:
                        question = textbox_question.get()
                        answer = textbox_answer.get()
                        va = '---'
                        qt = '1'
                        numq = numq + 1
                        c.execute("INSERT INTO TestText (id, testid, qtype, question, answer, varanswer) VALUES (?, ?, ?, ?, ?, ?)", (numq, testi, qt, question, answer, va,))
                        conn.commit()
                        c.execute("SELECT * FROM TestText")
                        print(c.fetchall())
                    if numq == kq:
                        window_fill_test.destroy()
                        info = Tk()
                        info.title("Ввод завершен")
                        info.geometry("200x100")

                        lbl = Label(info, text="Тест готов.")
                        lbl.grid(row=0, column=0, sticky=W + E)
                        lbl = Label(info, text="Теперь Вы можете пройти тест.")
                        lbl.grid(row=2, column=0, sticky=W + E)

            frame2.grid_remove()

            label = Label(frame1, text="Введите вопрос:")
            label.grid(row=0, column=0, sticky=W)
            label = Label(frame1, text="Введите верный ответ:")
            label.grid(row=1, column=0, sticky=W)

            textbox_question = Entry(frame1, width=50)
            textbox_question.focus_set()
            textbox_question.grid(row=0, column=1, sticky=W)
            textbox_answer = Entry(frame1, width=10)
            textbox_answer.grid(row=1, column=1, sticky=W)

            button_next_question = Button(frame1, text="Следующий вопрос", command=next_question_1)
            button_next_question.grid(row=4, column=1, padx=20, pady=10, sticky=W + E)
            frame1.grid(row=2, column=0, columnspan=3)

        def select_answers():
            def next_question_2():
                sql = 'create table if not exists TestText (id INTEGER, testid INTEGER, qtype TEXT, question TEXT, answer TEXT, varanswer TEXT, PRIMARY KEY(id))'
                c.execute(sql)
                c.execute("SELECT * FROM TestText WHERE id = ?", (testi,))
                result = c.fetchall()
                for chk in result:
                    chkn = chk[2]
                if result == [] or chkn == 3:
                    numq = 1
                    q = textbox1.get()
                    va = textbox2.get()
                    a = textbox3.get()
                    qt = '2'
                    c.execute("INSERT INTO TestText (id, testid, qtype, question, answer, varanswer) VALUES (?,?,?,?,?,?)", (numq, testi, qt, q, a, va,))
                    conn.commit()
                    c.execute("SELECT * FROM TestText")
                    print(c.fetchall())
                else:
                    numq = 0
                    c.execute('''SELECT * FROM TestInfo WHERE id = ?;''', (testi,))
                    results = c.fetchall()
                    for result in results:
                        kq = int(result[2])
                    c.execute('''SELECT * FROM TestText WHERE testid = ?;''', (testi,))
                    results = c.fetchall()
                    for result in results:
                        numq = int(result[0])
                    if numq < kq:
                        q = textbox1.get()
                        a = textbox3.get()
                        va = textbox2.get()
                        qt = '2'
                        numq = numq + 1
                        c.execute(
                            "INSERT INTO TestText (id,testid,qtype,question,answer,varanswer) VALUES (?,?,?,?,?,?)", (numq, testi, qt, q, a, va,))
                        conn.commit()
                        c.execute("SELECT * FROM TestText")
                        print(c.fetchall())
                    if numq == kq:
                        window_fill_test.destroy()
                        info = Tk()
                        info.title("Ввод завершен")
                        info.geometry("200x100")

                        lbl = Label(info, text="Тест готов.")
                        lbl.grid(row=0, column=0, sticky=W + E)
                        lbl = Label(info, text="Теперь Вы можете пройти тест.")
                        lbl.grid(row=2, column=0, sticky=W + E)

            frame1.grid_remove()

            lbl = Label(frame2, text="Вопрос")
            lbl.grid(row=0, column=0, sticky=W)
            lbl = Label(frame2, text="Варианты ответов (через ;)")
            lbl.grid(row=1, column=0, sticky=W)
            lbl = Label(frame2, text="Верные ответы (слитно)")
            lbl.grid(row=2, column=0, sticky=W)

            textbox1 = Entry(frame2, width=50)
            textbox1.focus_set()
            textbox1.grid(row=0, column=1, sticky=W)
            textbox2 = Entry(frame2, width=50)
            textbox2.grid(row=1, column=1, sticky=W)
            textbox3 = Entry(frame2, width=10)
            textbox3.grid(row=2, column=1, sticky=W)

            butnextq = Button(frame2, text="Следующий вопрос", command=next_question_2)
            butnextq.grid(row=4, column=1, padx=20, pady=10, sticky=W + E)
            frame2.grid(row=3, column=0, columnspan=3)

        qt = IntVar()
        rbtn1 = Radiobutton(window_fill_test, text="Один ответ", value=1, variable=qt, command=input_answers)
        rbtn2 = Radiobutton(window_fill_test, text="Несколько ответов", value=2, variable=qt, command=select_answers)
        rbtn1.grid(row=1, column=1, padx=10, pady=20)
        rbtn2.grid(row=1, column=2, padx=10, pady=20)

    window_create = Tk()
    window_create.title("Создание теста")
    window_create.resizable(False, False)

    frame_create_test_width = 480
    frame_create_test_height = 150

    frame_create_test = Frame(window_create)
    frame_create_test.config(width=frame_create_test_width, height=frame_create_test_height)
    frame_create_test.pack()
    window_create.config(width=frame_create_test_width, height=frame_create_test_height)

    label = Label(frame_create_test, text="Создание теста")
    label.place(relx=0.5, rely=0.05, anchor="center")

    label = Label(frame_create_test, text="Название теста:")
    label.place(rely=0.15)

    label = Label(frame_create_test, text="Кол-во вопросов:")
    label.place(rely=0.35)

    label = Label(frame_create_test, text="Система оценивания:")
    label.place(rely=0.55)

    check_test_type = IntVar()
    radiobutton_score = Radiobutton(frame_create_test, text="Подсчёт баллов", variable=check_test_type, value=1)
    radiobutton_score.place(relx=0.3, rely=0.55)
    radiobutton_rating = Radiobutton(frame_create_test, text="Выставление оценки", variable=check_test_type, value=2)
    radiobutton_rating.place(relx=0.6, rely=0.55)

    textbox_name = Entry(frame_create_test, width=50)
    textbox_name.focus_set()
    textbox_name.place(relx=0.3, rely=0.15)

    textbox_questions_number = Entry(frame_create_test, width=10)
    textbox_questions_number.place(relx=0.3, rely=0.35)

    button_next = Button(frame_create_test, text="Далее", width=50, command=next_step)
    button_next.place(relx=0.5, rely=0.85, anchor="center")


def execute_test():
    execute = Tk()
    execute.title("Прохождение теста")
    execute.geometry("500x100")

    lbl = Label(execute, text="Введите название существующего теста и свое имя")
    lbl.grid(row=0, column=1, sticky=W)

    lbl = Label(execute, text="Название теста:")
    lbl.grid(row=3, column=0, sticky=W)
    textbox1 = Entry(execute, width=50)
    textbox1.grid(row=3, column=1, sticky=W)

    lbl = Label(execute, text="Ваше имя:")
    lbl.grid(row=4, column=0, sticky=W)
    textbox2 = Entry(execute, width=50)
    textbox2.grid(row=4, column=1, sticky=W)

    sql = 'CREATE TABLE IF NOT EXISTS TestAnswers (id INTEGER, name TEXT, uanswer TEXT, testid INTEGER, umark INTEGER)'
    c.execute(sql)
    conn.commit()

    def start_test():
        def next_answer():
            def nextaa():
                c.execute('''SELECT * FROM TestText WHERE testid = ? AND id = ?;''', (takenid, ida))
                results = c.fetchall()
                for result in results:
                    questiontype = int(result[2])

                if questiontype == 1:
                    uans = textbox1.get()

                if questiontype == 2:
                    for i in range(1, varanswer_amount + 1):
                        uans += str(varans[i])
                    uans = uans.replace('0', '')

                c.execute("INSERT INTO TestAnswers (name,id) VALUES (?,?)", (username, ida))
                conn.commit()
                c.execute("UPDATE TestAnswers SET testid = ? WHERE name = ?", (takenid, username,))
                conn.commit()
                c.execute("UPDATE TestAnswers SET uanswer = ? WHERE id = ? AND name = ?", (uans, ida, username,))
                conn.commit()

                if ida == kq:
                    start.destroy()
                    resulttest = Tk()
                    resulttest.title("Результаты")
                    resulttest.geometry("300x200")
                    x = (resulttest.winfo_screenwidth() - resulttest.winfo_reqwidth()) / 4
                    y = (resulttest.winfo_screenheight() - resulttest.winfo_reqheight()) / 4
                    resulttest.wm_geometry("+%d+%d" % (x, y))

                    lbl = Label(resulttest, text="Тест пройден!", font=("Comic Sans", 20, "bold"))
                    lbl.grid(row=0, column=0, sticky=W)
                    i = 1
                    point = 0
                    while (i <= kq):
                        c.execute('''SELECT * FROM TestText WHERE id = ?;''', (i,))
                        que = c.fetchall()
                        for correcta in que:
                            correctans = correcta[4]
                        c.execute('''SELECT * FROM TestAnswers WHERE id = ? AND name = ?;''', (i, username,))
                        ans = c.fetchall()
                        for ua in ans:
                            yourans = ua[2]
                        if correctans == yourans:
                            point = point + 1
                        i = i + 1
                    c.execute('''SELECT * FROM TestInfo WHERE name = ?;''', (namecheck,))
                    results = c.fetchall()
                    for result in results:
                        typemark = result[3]
                    if typemark == '1':
                        lbl2 = Label(resulttest, text="Правильных ответов: ", font=("Comic Sans", 10, "bold"))
                        lbl2.grid(row=1, column=0, pady=50, sticky=W)
                        lbl2 = Label(resulttest, text=point, font=("Comic Sans", 12, "bold"))
                        lbl2.grid(row=1, column=0, sticky=E)
                        mark = point
                        c.execute('''UPDATE TestAnswers SET umark = ? WHERE name = ?;''', (mark, username,))
                        conn.commit()
                        z = 0
                        c.execute("INSERT INTO TestAnswers (id,name,uanswer,testid,umark) VALUES (?,?,?,?,?)",
                                  (z, '-', '-', takenid, '-'))
                        conn.commit()
                        w = 3
                        c.execute("INSERT INTO TestText (testid,qtype,question,answer,varanswer) VALUES (?,?,?,?,?)",
                                  (takenid, w, '-', '-', '-',))
                        conn.commit()

                    else:
                        mark = (point / kq) * 100
                        if mark < 50:
                            mark = 2
                        if (mark >= 50) and (mark < 75):
                            mark = 3
                        if (mark >= 75) and (mark < 90):
                            mark = 4
                        if (mark >= 90) and (mark <= 100):
                            mark = 5
                        c.execute('''UPDATE TestAnswers SET umark = ? WHERE name = ?;''', (mark, username,))
                        conn.commit()
                        z = 0
                        c.execute("INSERT INTO TestAnswers (id,name,uanswer,testid,umark) VALUES (?,?,?,?,?)",
                                  (z, '-', '-', takenid, '-'))
                        conn.commit()
                        w = 3
                        c.execute("INSERT INTO TestText (testid,qtype,question,answer,varanswer) VALUES (?,?,?,?,?)",
                                  (takenid, w, '-', '-', '-',))
                        conn.commit()

                        lbl2 = Label(resulttest, text="Оценка: ", font=("Comic Sans", 10, "bold"))
                        lbl2.grid(row=1, column=0, sticky=W)
                        lbl2 = Label(resulttest, text=mark, font=("Comic Sans", 12, "bold"))
                        lbl2.grid(row=1, column=0, sticky=E)
                else:
                    start.withdraw()
                    next_answer()

            start = Tk()
            start.title("Вопрос")
            start.geometry("600x400")
            x = (start.winfo_screenwidth() - start.winfo_reqwidth()) / 4
            y = (start.winfo_screenheight() - start.winfo_reqheight()) / 4
            start.wm_geometry("+%d+%d" % (x, y))

            execute.withdraw()

            c.execute('''SELECT * FROM TestInfo WHERE id = ?;''', (takenid,))
            results = c.fetchall()
            for result in results:
                kq = int(result[2])

            c.execute('''SELECT * FROM TestAnswers WHERE testid = ?;''', (takenid,))
            result = c.fetchall()
            for ch in result:
                chpr = ch[0]
            if result == [] or chpr == '0':
                ida = 1
            else:
                c.execute('''SELECT * FROM TestAnswers WHERE testid = ?;''', (takenid,))
                result = c.fetchall()
                for i in result:
                    ida = int(i[0])
                ida = ida + 1
            c.execute('''SELECT * FROM TestText WHERE testid = ? AND id = ?;''', (takenid, ida))
            results = c.fetchall()
            for result in results:
                numq = int(result[0])
                questiontype = int(result[2])
                question = str(result[3])
                answer = str(result[4])
                varanswer = str(result[5])

            label = Label(start, text="Всего вопросов: ", font=("Comic Sans", 10))
            label.grid(row=0, column=0, sticky=W)
            label = Label(start, text=ida, font=("Arial", 10, "italic"))
            label.grid(row=0, column=1, sticky=W)

            label = Label(start, text="Номер текущего вопроса: ", font=("Arial", 10))
            label.grid(row=1, column=0, sticky=W)
            label = Label(start, text=kq, font=("Arial", 10, "italic"))
            label.grid(row=1, column=1, sticky=W)

            label = Label(start, text=" ")
            label.grid(row=2, column=0)

            label = Label(start, text=question, font=("Arial", 12,))
            label.grid(row=3, column=1, sticky=W)
            label = Label(start, text="Вопрос:", font=("Arial", 12))
            label.grid(row=3, column=0, sticky=W + E)
            label = Label(start, text=" ")
            label.grid(row=4, column=0)

            if questiontype == 1:
                label = Label(start, text="Ответ:", font=("Arial", 12))
                label.grid(row=5, column=0, sticky=W + E)
                textbox1 = Entry(start, width=50)
                textbox1.focus_set()
                textbox1.grid(row=5, column=1, sticky=W)

            if questiontype == 2:
                label = Label(start, text="Ответ:", font=("Arial", 12))
                label.grid(row=5, column=0, sticky=W + E)
                varanswer = varanswer.split(';')
                varanswer_amount = len(varanswer)

                varans = [None] * varanswer_amount
                check_buttons = [None] * varanswer_amount
                for i in range(1, varanswer_amount + 1):
                    varans[i] = StringVar()
                    varans[i].set('0')
                    check_buttons[i] = Checkbutton(start, text=varanswer[i], variable=varans[i], onvalue=str(i), offvalue='0', command=lambda: varans[i].set(str(i)))
                    check_buttons[i].grid(rows=(4 + i), column=1, sticky=W)

            label = Label(start, text=" ")
            label.grid(row=9, column=0)
            butnextq = Button(start, text="Далее", font=("Comic Sans", 10, "bold"), command=nextaa)
            butnextq.grid(row=10, column=1, sticky=W + E)

        namecheck = textbox1.get()
        username = textbox2.get()
        results = c.execute('''SELECT * FROM TestInfo WHERE name = ?;''', (namecheck,)).fetchall()

        for i in results:
            takenid = i[0]
        if results == []:
            error = Tk()
            error.title("Ошибка")
            error.geometry("300x100")
            x = (error.winfo_screenwidth() - error.winfo_reqwidth()) / 4
            y = (error.winfo_screenheight() - error.winfo_reqheight()) / 4
            error.wm_geometry("+%d+%d" % (x, y))

            lbl = Label(error, text="Тест не найден.")
            lbl.grid(row=0, column=1, sticky=W + E)
            lbl = Label(error, text="Попробуйте ввести название теста снова.")
            lbl.grid(row=1, column=1, sticky=W + E)
        else:
            next_answer()

    butnext = Button(execute, text="Начать", command=start_test)
    butnext.grid(row=5, column=1, sticky=W + E)


def check_results():
    def check_test():
        test_name = textbox_test_name.get()
        test_id = textbox_test_id.get()

        results = c.execute('SELECT * FROM TestInfo WHERE name = ?', (test_name,)).fetchall()
        for result in results:
            testid = result[0]

        results = c.execute('SELECT * FROM TestAnswers WHERE name = ? AND testid = ?', (test_id, testid,)).fetchall()
        for result in results:
            mark = result[4]

        window_check_test = Tk()
        window_check_test.title("Данны об участнике " + test_id)
        window_check_test.geometry("800x600")

        label = Label(window_check_test, text='Последняя оценка по тесту: ')
        label.grid(row=0, column=0, sticky=W)
        label = Label(window_check_test, text=test_name)
        label.grid(row=1, column=0, sticky=W)
        label = Label(window_check_test, text=mark)
        label.grid(row=1, column=1, sticky=W)
        label = Label(window_check_test, text='Ответы ' + test_id + ':')
        label.grid(row=2, column=0, sticky=W)

        count = 3
        for i in results:
            label = Label(window_check_test, text='Вопрос ' + str(i[0]) + ': ' + str(i[2]))
            label.grid(row=count, column=2, sticky=W)
            count += 1

    def check_history():
        window_check_history = Tk()
        window_check_history.title("История прохождения тестов")
        window_check_history.geometry("800x600")

        count = 1
        result = c.execute('SELECT * FROM TestAnswers').fetchall()
        for i in result:
            label = Label(window_check_history, text=i)
            label.grid(row=count, column=0, sticky=W + E)
            count += 1

        # label = Label(window_check_next_text, text=results)
        # label.grid(row=2, column=0, sticky=W + E)

    def check_answers():
        window_check_answers = Tk()
        window_check_answers.title("Ответы на тесты")
        window_check_answers.geometry("800x600")

        results = c.execute('SELECT * FROM TestText').fetchall()

        count = 1
        for i in results:
            label = Label(window_check_answers, text=i)
            label.grid(row=count, column=0, sticky=W + E)
            count += 1

    check = Tk()
    check.title("Результаты")
    check.geometry("500x150")

    label = Label(check, text="Название пройденного теста:")
    label.grid(row=2, column=0, sticky=W)
    textbox_test_name = Entry(check, width=50)
    textbox_test_name.grid(row=2, column=1, sticky=W)

    label = Label(check, text="Имя проходившего:")
    label.grid(row=3, column=0, sticky=W)
    textbox_test_id = Entry(check, width=50)
    textbox_test_id.grid(row=3, column=1, sticky=W)

    button_check_test = Button(check, text="Результаты по введенным данным", command=check_test)
    button_check_test.grid(row=4, column=1, sticky=W + E)

    button_check_history = Button(check, text="Посмотреть историю всех прохождений", command=check_history)
    button_check_history.grid(row=5, column=1, sticky=W + E)

    button_check_answers = Button(check, text="Посмотреть ответы на тесты", command=check_answers)
    button_check_answers.grid(row=6, column=1, sticky=W + E)


window = Tk()
window.title("Тесты")
window.geometry("240x120")
window.resizable(False, False)

button_create = Button(window, text="Создать тест", width=20, command=create_test)
button_create.place(relx=0.2, rely=0.1)

button_start = Button(window, text="Пройти тест", width=20, command=execute_test)
button_start.place(relx=0.2, rely=0.4)

button_check = Button(window, text="Результаты", width=20, command=check_results)
button_check.place(relx=0.2, rely=0.7)

window.mainloop()
