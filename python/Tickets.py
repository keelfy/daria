from tkinter import *
import sqlite3


# функция отвечает за добавление билета в базу данных и открывает окно купленного билета
def append():
    # без global в этой функции нельзя будет менять эти переменные
    global place

    # получаем текущие значения в полях ввода по выбранному типу билета
    # ticket_type = 0 - взрослый билет
    # ticket_type = 1 - льготый
    # ticket_type = 2 - детский
    ticket_type = check_type.get()
    passport = textbox_passport[ticket_type].get()
    name = textbox_name[ticket_type].get()
    gender = check_gender[ticket_type].get()
    age = textbox_age[ticket_type].get()
    # document - эта переменная отвечает за доп. документ, который есть у льготного типа билета или детского
    # у взрослого (ticket type id = 0) эта переменная всегда пустая
    document = ""
    if ticket_type != 0:
        document = textbox_document[ticket_type].get()
    # дату получаем из поля ввода в главном окне
    date = textbox_date.get()

    # проверяем, если основные поля у билета не заполненны, то не даем купить билет
    if passport == "" or name == "" or age == "":
        print("Необходимо заполнить все поля!")
        return

    # это три проверки на то, существуют ли уже билеты с такими данным на этот поезд
    if ticket_type == 0:
        # этот запрос возвращает кол-во билетов,
        # у которого которые равны текущим значениям passport, travelFrom, travelTo, date
        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE passport=? AND travel_from=? AND travel_to=? AND date=?", [passport, cities[travelFrom], cities[travelTo], date]).fetchone()[0]
        # если есть хоть 1 такой билет, то говорим что нельзя
        if check != 0:
            print("Билет на паспорт " + passport + " уже куплен на этот поезд!")
            return
    elif ticket_type == 1:
        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE document=? AND travel_from=? AND travel_to=? AND date=?", [document, cities[travelFrom], cities[travelTo], date]).fetchone()[0]
        if check != 0:
            print("Билет с этим льготным документом " + passport + " уже куплен на этот поезд!")
            return
    elif ticket_type == 2:
        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE document=? AND travel_from=? AND travel_to=? AND date=?", [document, cities[travelFrom], cities[travelTo], date]).fetchone()[0]
        if check != 0:
            print("Билет с этим свидетельством рождения " + passport + " уже куплен на этот поезд!")
            return

    if ticket_type == 2:
        parent = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE passport=?", [passport]).fetchone()[0]
        if parent <= 0:
            print("Паспорт взрослого не найден! Регистрация не завершена!")
            return

    # посылаю запрос в базу данных, добавляя новую запись билета со всеми данными
    query = 'insert into prof values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    c.execute(query, (ticket_type, passport, name, gender, age, place, cities[travelFrom], cities[travelTo], date, document))
    conn.commit()
    # присваиваю значению места текущее кол-во записей в бд
    place = c.execute("SELECT COUNT(*) FROM " + table_name).fetchone()[0]
    # выводим имя купившего
    print("Билет на имя " + name + " куплен!")
    # переключаем окно на окно купленного билета
    switch_to_bought()


# эта функция заполняет все поля в окне печати билета и открывает окошко печати
def print_ticket():
    global place
    # получаем необходимые данные из полей ввода
    ticket_type = check_type.get()
    passport = textbox_passport[ticket_type].get()
    name = textbox_name[ticket_type].get()
    age = textbox_age[ticket_type].get()
    # заполняем поле document, если билет детский или льготный
    document = ""
    if ticket_type == 1:
        document = "Льгота: " + textbox_document[ticket_type].get()
    elif ticket_type == 2:
        document = "Св. о рождении: " + textbox_document[ticket_type].get()

    # заполняем все строки в окне данными
    # в наших вагонах по 54 места и считаем номер вагона и место в вагоне
    var_print_place.set("Место: " + str(place % 54))
    var_print_carriage.set("Вагон: " + str(place // 54 + 1))
    var_print_document.set(document)
    if ticket_type == 2:
        var_print_passport.set("Паспорт взрослого: " + passport)
    else:
        var_print_passport.set("Паспорт: " + passport)
    var_print_number.set("Билет №" + str(place))
    var_print_age.set("Возраст: " + age)
    var_print_date.set(textbox_date.get())
    var_print_from.set(cities[travelFrom])
    var_print_to.set(cities[travelTo])
    var_print_type.set(ticket_type_names[ticket_type])
    var_print_name.set("ФИО: " + name)

    # выключаем окно купленного билета и включаем окно печати
    frame_bought.pack_forget()
    frame_print.pack()


# эта функция переключаем окно на окно купленного билета
def switch_to_bought():
    # устанавливаем заголовок окнку в виде маршрута следования, для красоты
    root.title(cities[travelFrom] + " -> " + cities[travelTo] + ": " + textbox_date.get())
    # получаем номер типа билета
    ticket_type = check_type.get()
    # Уставновка отображаемых данных о купленном билете
    var_bought_name.set("Имя владельца: " + textbox_name[ticket_type].get())
    var_bought_ticket_id.set("Номер места: " + str(place))
    var_bought_travel.set("Маршрут: из " + cities[travelFrom] + " в " + cities[travelTo] + ".  Дата: " + textbox_date.get())
    # выключаем на всякий случай все окна и включаем только окно купленного билета
    frame_print.pack_forget()
    frame_main.pack_forget()
    frame_buy.pack_forget()
    frame_bought.pack()


# эта функция открывает окно с заполнением данных и покупкой билета
def switch_to_buy():
    # проверяем, чтобы поезд не шел в город оправления и дата не была пустой
    if travelFrom == travelTo or travelTo == -1 or travelFrom == -1 or textbox_date.get() == "":
        print("По данному направлению поездов нет!")
        return

    # уставнавливаем заголовок окна красивый
    root.title(cities[travelFrom] + " -> " + cities[travelTo] + ": " + textbox_date.get())
    # включаем все окна кроме окна покупки билета
    frame_print.pack_forget()
    frame_bought.pack_forget()
    frame_main.pack_forget()
    frame_buy.pack()


# эта функция откраывает основное окно и очищает все поля ввода в окне покупки
def switch_to_main():
    # устаанвливаем заголовок главного окна
    root.title("Продажа билетов")

    # очистка всех полей ввода в окне покупки
    for i in range(3):
        textbox_age[i].delete(0, END)
    for i in range(3):
        textbox_name[i].delete(0, END)
    for i in range(3):
        textbox_passport[i].delete(0, END)
    for i in range(3):
        if i != 0:
            textbox_document[i].delete(0, END)

    # выключаем все, кроме главного окна
    frame_print.pack_forget()
    frame_bought.pack_forget()
    frame_buy.pack_forget()
    frame_main.pack()


# эта функция открывает окошко выбора места отправления
def add_from():
    window = Tk()
    window.resizable(False, False)
    window.title("Откуда?")
    listbox = Listbox(window, height=len(cities), width=40, selectmode=SINGLE)
    for i in cities:
        listbox.insert(END, i)
    listbox.bind("<<ListboxSelect>>", travel_from)
    listbox.pack()
    window.mainloop()


# эта функция устанавливает перемнной id выбранного пункта отправления
def travel_from(event):
    global travelFrom
    travelFrom = event.widget.curselection()[0]
    global var_travel_from
    var_travel_from.set(cities[travelFrom])


# эта функция устанавливает переменной id выбранного пункта назначения
def add_to():
    window_travel = Tk()
    window_travel.resizable(False, False)
    window_travel.title("Куда?")
    listbox2 = Listbox(window_travel, height=len(cities), width=40, selectmode=SINGLE)
    for i in cities:
        listbox2.insert(END, i)
    listbox2.bind("<<ListboxSelect>>", travel_to)
    listbox2.pack()
    window_travel.mainloop()


# эта функция открывает окошко выбора места назначения
def travel_to(event):
    global travelTo
    travelTo = event.widget.curselection()[0]
    global var_travel_to
    var_travel_to.set(cities[travelTo])


# эти переменные отвечают за id города в массиве городов. место назначения и прибытия
travelTo = -1
travelFrom = -1

# названия типов билетов нужном порядке. используется в окне печати
ticket_type_names = ["Взрослый", "Льготный", "Детский"]
# список доступных городов
cities = ["Санкт-Петербург", "Москва", "Сочи", "Казань", "Нижний новгород", "Воронеж", "Владивосток", "Тюмень", "Самара"]

# переменная, в которой хранится название файла с бд
location = 'data.db'
# здесь хранится название таблицы в бд
table_name = 'prof'

# объект-соединение, который дает возможность взаимодействовать с бд
conn = sqlite3.connect(location)
# курсор - отправляет запросы в бд
c = conn.cursor()

# запрос на создание бд, если её не существует
sql = 'create table if not exists ' + table_name + ' (type INTEGER, passport INTEGER, name TEXT, gender TEXT, age INTEGER, place INTEGER, travel_from TEXT, travel_to TEXT, date TEXT, document TEXT)'
c.execute(sql)

# устанавливаем начальное кол-во занятых мест запросом, который возвращает общее кол-во купленных билетов
place = c.execute("SELECT COUNT(*) FROM " + table_name).fetchone()[0]

# создаем основное окно
root = Tk()
root.resizable(False, False)
root.title("Продажа билетов")
root.geometry("640x480")

# START - Строка покупки билета
frame_main = Frame(root)
frame_main.config(width=640, height=480)
frame_main.pack()

label_header = Label(frame_main, text="Поиск билета по параметрам", bg="white")
label_header.place(relx=0.18, rely=0.4)
label_header.configure(font="Colibri, 25", fg="black")

var_travel_from = StringVar()
var_travel_from.set('-')
label_from = Label(frame_main, textvariable=var_travel_from, bg="#FFFFFF")
label_from.place(relx=0.4, rely=0.6)
button_from = Button(frame_main, text="Откуда", command=add_from)
button_from.place(relx=0.3, rely=0.6)

var_travel_to = StringVar()
var_travel_to.set('-')
label_to = Label(frame_main, textvariable=var_travel_to, bg="#FFFFFF")
label_to.place(relx=0.4, rely=0.7)
button_to = Button(frame_main, text="Куда", command=add_to)
button_to.place(relx=0.32, rely=0.7)

label_date = Label(frame_main, text="Дата (дд:мм:гг)", bg="#FFFFFF")
label_date.place(relx=0.6, rely=0.6)
textbox_date = Entry(frame_main, width=20)
textbox_date.place(relx=0.6, rely=0.65)

button_search = Button(frame_main, text="Купить билет", command=switch_to_buy, width=86)
button_search.place(relx=0.02, rely=0.85)
# END - Строка покупки билета

# START - Начальные координаты смещения
y = 0.02
dis_y = 0.06
x = 0.02
# END - Начальные координаты смещения

# START - Покупка обычного билета
frame_buy = Frame(root)
frame_buy.config(width=640, height=480)

check_type = IntVar()
label_passport = [None] * 3
label_name = [None] * 3
radiobutton_man = [None] * 3
radiobutton_woman = [None] * 3
label_age = [None] * 3
radiobutton_type = [None] * 3
textbox_passport = [None] * 3
textbox_name = [None] * 3
check_gender = [None] * 3
textbox_age = [None] * 3
textbox_document = [None] * 3

radiobutton_type_1 = Radiobutton(frame_buy, text="Обычный", value=0, variable=check_type)
radiobutton_type_1.place(relx=x, rely=y)

label_passport[0] = Label(frame_buy, text="Номер паспорта", bg="#FFFFFF")
label_passport[0].place(relx=x, rely=y + dis_y)
textbox_passport[0] = Entry(frame_buy, width=6)
textbox_passport[0].place(relx=x + 0.2, rely=y + dis_y)

label_name[0] = Label(frame_buy, text="ФИО", bg="#FFFFFF")
label_name[0].place(relx=x, rely=y + dis_y * 2)
textbox_name[0] = Entry(frame_buy, width=20)
textbox_name[0].place(relx=x + 0.2, rely=y + dis_y * 2)

check_gender[0] = IntVar()
check_gender[0].set(0)
radiobutton_man[0] = Radiobutton(frame_buy, text="Мужчина", value=0, variable=check_gender[0])
radiobutton_man[0].place(relx=x + 0.5, rely=y + dis_y * 1.5)
radiobutton_woman[0] = Radiobutton(frame_buy, text="Женщина", value=1, variable=check_gender[0])
radiobutton_woman[0].place(relx=x + 0.5, rely=y + dis_y * 2.5)

label_age[0] = Label(frame_buy, text="Возраст", bg="#FFFFFF")
label_age[0].place(relx=x, rely=y + dis_y * 3)
textbox_age[0] = Entry(frame_buy, width=3)
textbox_age[0].place(relx=x + 0.2, rely=y + dis_y * 3)
# END -  Покупка обычного билета

# START - Окно покупки билета
y = y + dis_y * 4 + 0.03

# Покупка льготного билета
radiobutton_type_2 = Radiobutton(frame_buy, text="Льготный", value=1, variable=check_type)
radiobutton_type_2.place(relx=x, rely=y)

label_passport[1] = Label(frame_buy, text="Номер паспорта", bg="#FFFFFF")
label_passport[1].place(relx=x, rely=y + dis_y)
textbox_passport[1] = Entry(frame_buy, width=6)
textbox_passport[1].place(relx=x + 0.2, rely=y + dis_y)

label_name[1] = Label(frame_buy, text="ФИО", bg="#FFFFFF")
label_name[1].place(relx=x, rely=y + dis_y * 2)
textbox_name[1] = Entry(frame_buy, width=20)
textbox_name[1].place(relx=x + 0.2, rely=y + dis_y * 2)

check_gender[1] = IntVar()
check_gender[1].set(0)
radiobutton_man[1] = Radiobutton(frame_buy, text="Мужчина", value=0, variable=check_gender[1])
radiobutton_man[1].place(relx=x + 0.5, rely=y + dis_y * 1.5)
radiobutton_woman[1] = Radiobutton(frame_buy, text="Женщина", value=1, variable=check_gender[1])
radiobutton_woman[1].place(relx=x + 0.5, rely=y + dis_y * 2.5)

label_age[1] = Label(frame_buy, text="Возраст", bg="#FFFFFF")
label_age[1].place(relx=x, rely=y + dis_y * 3)
textbox_age[1] = Entry(frame_buy, width=3)
textbox_age[1].place(relx=x + 0.2, rely=y + dis_y * 3)

label_document = Label(frame_buy, text="Льготный документ", bg="#FFFFFF")
label_document.place(relx=x, rely=y + dis_y * 4)
textbox_document[1] = Entry(frame_buy, width=20)
textbox_document[1].place(relx=x + 0.2, rely=y + dis_y * 4)
# Конец покупки льготного билета

y = y + dis_y * 5 + 0.03

# Покупка детского билета
radiobutton_type[2] = Radiobutton(frame_buy, text="Детский", value=2, variable=check_type)
radiobutton_type[2].place(relx=x, rely=y)

label_passport[2] = Label(frame_buy, text="Паспорт взрослого", bg="#FFFFFF")
label_passport[2].place(relx=x, rely=y + dis_y)
textbox_passport[2] = Entry(frame_buy, width=6)
textbox_passport[2].place(relx=x + 0.2, rely=y + dis_y)

label_name[2] = Label(frame_buy, text="ФИО", bg="#FFFFFF")
label_name[2].place(relx=x, rely=y + dis_y * 2)
textbox_name[2] = Entry(frame_buy, width=20)
textbox_name[2].place(relx=x + 0.2, rely=y + dis_y * 2)

check_gender[2] = IntVar()
check_gender[2].set(0)
radiobutton_man[2] = Radiobutton(frame_buy, text="Мальчик", value=0, variable=check_gender[2])
radiobutton_man[2].place(relx=x + 0.5, rely=y + dis_y * 1.5)
radiobutton_woman[2] = Radiobutton(frame_buy, text="Девочка", value=1, variable=check_gender[2])
radiobutton_woman[2].place(relx=x + 0.5, rely=y + dis_y * 2.5)

label_age[2] = Label(frame_buy, text="Возраст", bg="#FFFFFF")
label_age[2].place(relx=x, rely=y + dis_y * 3)
textbox_age[2] = Entry(frame_buy, width=3)
textbox_age[2].place(relx=x + 0.2, rely=y + dis_y * 3)

label_child_document = Label(frame_buy, text="Св. о рождении", bg="#FFFFFF")
label_child_document.place(relx=x, rely=y + dis_y * 4)
textbox_document[2] = Entry(frame_buy, width=6)
textbox_document[2].place(relx=x + 0.2, rely=y + dis_y * 4)
# Конец покупки детского билета

button_append = Button(frame_buy, text="Купить", command=append, width=86)
button_append.place(relx=x, rely=y + dis_y * 5)
# END - Окно покупки билета

# START - Окно купленного билета
frame_bought = Frame(root)
frame_bought.config(width=640, height=480)

label_bought = Label(frame_bought, text="Вы приобрели билет!", bg="white")
label_bought.place(relx=0.25, rely=0.2)
label_bought.configure(font="Colibri, 25", fg="black")

var_bought_name = StringVar()
var_bought_name.set('-')
label_bought_name = Label(frame_bought, textvariable=var_bought_name, bg="#FFFFFF")
label_bought_name.place(relx=0.1, rely=0.4)

var_bought_ticket_id = StringVar()
var_bought_ticket_id.set('-')
label_bought_ticket_id = Label(frame_bought, textvariable=var_bought_ticket_id, bg="#FFFFFF")
label_bought_ticket_id.place(relx=0.1, rely=0.48)

var_bought_travel = StringVar()
var_bought_travel.set('-')
label_bought_travel = Label(frame_bought, textvariable=var_bought_travel, bg="#FFFFFF")
label_bought_travel.place(relx=0.1, rely=0.56)

button_bought_print = Button(frame_bought, text="Печать билета", command=print_ticket)
button_bought_print.place(relx=0.1, rely=0.64)

button_bought_main = Button(frame_bought, text="Выбор другого поезда", command=switch_to_main)
button_bought_main.place(relx=0.1, rely=0.72)

button_back_to_buy = Button(frame_bought, text="Купить ещё один билет на этот поезд", command=switch_to_buy, width=86)
button_back_to_buy.place(relx=0.02, rely=0.9)
# END - Окно купленного билета

# START - Окно печати билета
frame_print = Frame(root)
frame_print.config(width=640, height=480)

canvas = Canvas(frame_print, width=640, height=480)
canvas.create_rectangle(10, 10, 620, 400, outline='black', fill='white', width=2)
canvas.place(relx=0, rely=0)

font = "Colibri, 12"

var_print_number = StringVar()
label_print_number = Label(frame_print, textvariable=var_print_number, bg='white')
label_print_number.place(relx=0.1, rely=0.1)
label_print_number.configure(font="Colibri, 16", fg="black")

var_print_type = StringVar()
label_print_type = Label(frame_print, textvariable=var_print_type, bg='white')
label_print_type.place(relx=0.7, rely=0.1)
label_print_type.configure(font=font, fg="black")

var_print_name = StringVar()
label_print_name = Label(frame_print, textvariable=var_print_name, bg='white')
label_print_name.place(relx=0.1, rely=0.18)
label_print_name.configure(font=font, fg="black")

var_print_age = StringVar()
label_print_age = Label(frame_print, textvariable=var_print_age, bg='white')
label_print_age.place(relx=0.1, rely=0.26)
label_print_age.configure(font=font, fg="black")

var_print_passport = StringVar()
label_print_passport = Label(frame_print, textvariable=var_print_passport, bg='white')
label_print_passport.place(relx=0.1, rely=0.34)
label_print_passport.configure(font=font, fg="black")

var_print_document = StringVar()
label_print_document = Label(frame_print, textvariable=var_print_document, bg='white')
label_print_document.place(relx=0.5, rely=0.34)
label_print_document.configure(font=font, fg="black")

label_print_from = Label(frame_print, text="Откуда:", bg='white')
label_print_from.place(relx=0.1, rely=0.42)
label_print_from.configure(font=font, fg="black")

var_print_from = StringVar()
label_print_var_from = Label(frame_print, textvariable=var_print_from, bg='white')
label_print_var_from.place(relx=0.6, rely=0.42)
label_print_var_from.configure(font=font, fg="black")

label_print_to = Label(frame_print, text="Куда:", bg='white')
label_print_to.place(relx=0.1, rely=0.5)
label_print_to.configure(font=font, fg="black")

var_print_to = StringVar()
label_print_var_to = Label(frame_print, textvariable=var_print_to, bg='white')
label_print_var_to.place(relx=0.6, rely=0.5)
label_print_var_to.configure(font=font, fg="black")

label_print_date = Label(frame_print, text="Дата отправления:", bg='white')
label_print_date.place(relx=0.1, rely=0.58)
label_print_date.configure(font=font, fg="black")

var_print_date = StringVar()
label_print_var_date = Label(frame_print, textvariable=var_print_date, bg='white')
label_print_var_date.place(relx=0.6, rely=0.58)
label_print_var_date.configure(font=font, fg="black")

var_print_place = StringVar()
label_print_place = Label(frame_print, textvariable=var_print_place, bg='white')
label_print_place.place(relx=0.1, rely=0.66)
label_print_place.configure(font=font, fg="black")

var_print_carriage = StringVar()
label_print_carriage = Label(frame_print, textvariable=var_print_carriage, bg='white')
label_print_carriage.place(relx=0.5, rely=0.66)
label_print_carriage.configure(font=font, fg="black")

button_print_back = Button(frame_print, text="Назад", command=switch_to_bought)
button_print_back.place(relx=0.9, rely=0.9)
# END - Окно печати билета

root.mainloop()
conn.close()
