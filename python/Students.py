import sqlite3


def add_student():
    print("--- Добавление нового студента ---")

    result = 0
    while result == 0:
        name = input("Введите имя => ")
        document = input("Введите номер студенческого билета => ")
        speciality = input("Введите специальность => ")
        group = input("Введите номер группы => ")

        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE document='" + document + "'").fetchone()[0]

        if check == 0:
            result = 1
        else:
            print("Студент с таким студенческим билетом уже существует!\n1. Отменить ввод\n2. Повторить ввод")
            if int(input()) == 1:
                result = 2

    if result == 2:
        return

    query = 'INSERT INTO ' + table_name + ' VALUES(?, ?, ?, ?)'
    c.execute(query, (name, group, document, speciality))
    conn.commit()

    print("Студент " + name + " добавлен!\n")


def remove_student():
    print("--- Исключение студента ---")

    result = 0
    while result == 0:
        document = input("Введите номер студенческого билета студента => ")

        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE document='" + document + "'").fetchone()[0]

        if check == 0:
            print("Студент с таким студенческим билетом не найден!\n1. Отменить ввод\n2. Повторить ввод")
            if int(input()) == 1:
                result = 2
        else:
            result = 1

    if result == 2:
        return

    query = 'DELETE FROM ' + table_name + " WHERE document='" + document + "'"
    c.execute(query)
    conn.commit()

    print("Студент исключен!\n")


def move_student():
    print("--- Перевод студента ---")

    result = 0
    while result == 0:
        document = input("Введите номер студенческого билета студента => ")
        speciality = input("Введите новую специальность => ")
        group = input("Введите новый номер группы => ")

        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE document='" + document + "'").fetchone()[0]

        if check == 0:
            print("Студент с таким студенческим билетом не найден!\n1. Отменить ввод\n2. Повторить ввод")
            if int(input()) == 1:
                result = 2
        else:
            result = 1

    if result == 2:
        return

    query = 'UPDATE ' + table_name + " SET speciality='" + speciality + "', st_group='" + group + "' WHERE document='" + document + "'"
    c.execute(query)
    conn.commit()
    print("Студент переведен в группу " + group + " на специальность " + speciality + "\n")


def find_student():
    print("--- Поиск студента ---")

    print("По какому признаку искать студентов?\n1. Студенческий\n2. Имя\n3. Группа\n4. Специальность")
    action = int(input())

    if action == 2:
        name = input("Введите имя => ")
        query = c.execute('SELECT * FROM ' + table_name + " WHERE name='" + name + "'")
    elif action == 3:
        group = input("Введите группу => ")
        query = c.execute('SELECT * FROM ' + table_name + " WHERE st_group='" + group + "'")
    elif action == 4:
        speciality = input("Введите специальность => ")
        query = c.execute('SELECT * FROM ' + table_name + " WHERE speciality='" + speciality + "'")
    else:
        document = input("Введите номер студенческого билета => ")
        query = c.execute('SELECT * FROM ' + table_name + " WHERE document='" + document + "'")

    conn.commit()

    count = 1
    line = '-' * 73
    print(line)
    print("{:^3s}{:^30s}{:^15s}{:^15s}{:^10s}".format("№", "Имя", "Студенческий", "Специальность", "Группа"))
    print(line)
    for row in c:
        print("{:^3d}{:<30s}{:^15s}{:^15s}{:^10s}".format(count, row[0], row[2], row[3], row[1]))
        count += 1
    print(line)


def find_group():
    print("--- Поиск группы ---")

    result = 0
    while result == 0:
        group = input("Введите номер группы => ")
        speciality = input("Введите специальность => ")

        check = c.execute("SELECT COUNT(*) FROM " + table_name + " WHERE st_group='" + group + "' AND speciality='" + speciality + "'").fetchone()[0]

        if check == 0:
            print("Группа не найдена!\n1. Отменить ввод\n2. Повторить ввод")
            if int(input()) == 1:
                result = 2
        else:
            result = 1

    if result == 2:
        return

    query = c.execute('SELECT name, document FROM ' + table_name + " WHERE st_group='" + group + "' AND speciality='" + speciality + "'")
    conn.commit()

    count = 1
    line = '-' * 45
    print(line)
    print("{:<30s}{:^15s}".format("Имя", "Студенческий"))
    print(line)
    for row in c:
        print("{:<30s}{:^15s}".format(row[0], row[1]))
        count += 1
    print(line)


def print_all():
    query = c.execute('SELECT * FROM ' + table_name)
    conn.commit()
    count = 1
    line = '-' * 73
    print(line)
    print("{:^3s}{:^30s}{:^15s}{:^15s}{:^10s}".format("№", "Имя", "Студенческий", "Специальность", "Группа"))
    print(line)
    for row in c:
        print("{:^3d}{:<30s}{:^15s}{:^15s}{:^10s}".format(count, row[0], row[2], row[3], row[1]))
        count += 1
    print(line)


location = 'students.db'
table_name = 'prof'

conn = sqlite3.connect(location)
c = conn.cursor()

sql = 'create table if not exists ' + table_name + ' (name TEXT, st_group TEXT, document TEXT, speciality TEXT)'
c.execute(sql)

action = 0

while True:
    print("Что вы хотите сделать?\n"
          "1. Добавить студента\n"
          "2. Исключить студента\n"
          "3. Перевести студента\n"
          "4. Найти студента\n"
          "5. Найти группу\n"
          "6. Вывести всю информацию\n"
          "7. Выйти из программы")

    action = int(input())

    if action == 1:
        add_student()
    elif action == 2:
        remove_student()
    elif action == 3:
        move_student()
    elif action == 4:
        find_student()
    elif action == 5:
        find_group()
    elif action == 6:
        print_all()
    elif action == 7:
        break
