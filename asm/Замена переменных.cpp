#include <iostream>

using namespace std; 

int main() 
{ 
	setlocale(LC_ALL, "rus"); 
	char f, s; 

	cout << "Введите первый символ => "; 
	cin >> f; 
	cout << "Введите второй символ => "; 
	cin >> s; 

	_asm 
	{ 
		mov AL, f 
		mov AH, s 
		xchg AL, AH 
		mov f, AL 
		mov s, AH 
	} 

	cout << "Результат = " << f << s << endl;
	system("pause");
}