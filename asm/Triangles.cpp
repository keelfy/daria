﻿#include <iostream>

bool isPossible(int a, int b, int c) {
	int result = 0;
	_asm {
		mov eax, a
		mov ecx, c
		cmp eax, 0
		jng end
		cmp ebx, 0
		jng end
		cmp ecx, 0
		jng end
		add eax, b
		cmp eax, ecx
		jle end
		acb:
			mov eax, a
			mov ecx, c
			add eax, ecx
			cmp eax, b
			jle end
		cba:
			mov ebx, b
			mov ecx, c
			add ebx, ecx
			cmp ebx, a
			jle end
			mov result, 1
			end:
	}
	return result == 1;
}

int get_type(int a, int b, int c) {
	int result;
	_asm {
		mov eax, a
		mov ebx, b
		mov ecx, c
		imul eax, eax
		imul ebx, ebx
		imul ecx, ecx
		add eax, ebx
		sub eax, ecx
		cmp eax, 0
		jg ostriy
		je pryamoy
		jl tupoy
		ostriy :
		mov result, 0
			jmp end1
			pryamoy :
		mov result, 1
			jmp end1
			tupoy :
		mov result, 2
			jmp end1
			end1 :
	}
	return result;
}

bool isEquilateral(int a, int b, int c) {
	int result = 0;
	_asm {
		mov eax, a
		mov ebx, b
		mov ecx, c
		cmp eax, ebx
		je next1
		jmp end2
		next1 :
		cmp ebx, ecx
			je next2
			jmp end2
			next2 :
		cmp ecx, eax
			je right
			jmp end2
			right :
		mov result, 1
			end2 :
	}
	return result == 1;
}

bool isIsosceles(int a, int b, int c) {
	int result = 0;
	_asm {
		mov eax, a
		mov ebx, b
		mov ecx, c
		cmp eax, ebx
		je right
		next1 :
		cmp ebx, ecx
			je right
			next2 :
		cmp ecx, eax
			je right
			jmp end2
			right :
		mov result, 1
			end2 :
	}
	return result == 1;
}

int main()
{
	setlocale(0, "");

	int a, b, c;
	int result = 0;

	std::cout << "Введите длины сторон треугольника => ";
	std::cin >> a >> b >> c;

	if (isPossible(a, b, c)) {
		std::cout << "Треугольник существует." << std::endl;
	}
	else {
		std::cout << "Треугольник не существует." << std::endl;
		return 0;
	}

	switch (get_type(a, b, c)) {
	case 0:
		std::cout << "Треугольник остроугольный." << std::endl;
		break;
	case 1:
		std::cout << "Треугольник прямоугольный." << std::endl;
		break;
	case 2:
		std::cout << "Треугольник тупоугольный." << std::endl;
		break;
	}

	if (isEquilateral(a, b, c)) {
		std::cout << "Треугольник равносторонний." << std::endl;
	}
	else if (isIsosceles(a, b, c)) {
		std::cout << "Треугольник равнобедренный." << std::endl;
	}
	else {
		std::cout << "Треугольник произвольный." << std::endl;
	}
}
