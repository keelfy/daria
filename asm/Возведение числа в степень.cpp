﻿#include <iostream> 

using namespace std; 

int main()
{ 
	setlocale(LC_ALL, "rus"); 
	int a, n, ans; 
	cout << "Введите число, которое хтите возвести в степень => "; 
	cin >> a; 
	cout << "Введиет степень, в которую вы хотите возвести число => "; 
	cin >> n; 

	_asm 
	{ 
		mov EAX, a 
		mov EBX, a 
		mov ECX, n 
		sub ECX, 1 
		metka: 
		mul EBX 
		loop metka 
		mov ans, EAX 
	} 

	cout << a << " ^ " << n << " = " << ans;
	system("pause"); 
}