﻿#include <iostream>

int main()
{
	int a, b, c;
	int d = 0;
	int koren1 = 0, ostatok1 = 0;
	int koren2 = 0, ostatok2 = 0;

	setlocale(LC_ALL, "ru-RU");
	std::cout << "Введите переменные квадратного уравнения - a, b и c => ";
	std::cin >> a;
	std::cin >> b;
	std::cin >> c;

	_asm {
		diskr:
			mov eax, b
			imul eax, b
			mov ecx, 4
			imul ecx, a
			imul ecx, c
			sub eax, ecx
			cmp eax, 0
			je if_diskr_zero
		diskr_sqrt :
			mov ebx, 1
			// sqrt
			_sqrt :
				sub eax, ebx
				add ebx, 2
				cmp eax, 0
				jl sqrt_lower
				add d, 1
				jmp sqrt_end
			sqrt_lower :
				add d, 0
			sqrt_end :
				cmp eax, 0
				jg _sqrt
			jmp korni
		if_diskr_zero :
			mov d, 0
		korni :
			mov ecx, b
			neg ecx
			mov ebx, 2
			imul ebx, a
		koren1 :
			xor edx, edx
			mov eax, ecx
			add eax, d
			cdq
			idiv ebx
			push eax
			push edx
			cmp d, 0
			je korni_equals
		koren2 :
			xor edx, edx
			mov eax, ecx
			sub eax, d
			cdq
			idiv ebx
			push eax
			push edx
			jmp result
		korni_equals :
			push eax
			push edx
			jmp result
		result :
			pop edx
			pop eax
			mov koren1, eax
			mov ostatok1, edx
			pop edx
			pop eax
			mov koren2, eax
			mov ostatok2, edx
	}
	std::cout << "Корни квадратного уравнения:\n" << koren1 << " (Ост. = " << ostatok1 << ")\n" << koren2 << " (Ост. = " << ostatok2 << ")" << std::endl;

	system("pause");
	return 0;
}