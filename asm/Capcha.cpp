﻿#include <iostream>
#include <cstdlib>
#include <ctime>

int main()
{
	setlocale(LC_ALL, "ru-RU");
	srand(time(NULL));

	short v = (short) (rand() % 3);
	short a = (short) (rand() % 9 + 1);
	short b = (short) (rand() % 9 + 1);
	short c;
	short result = 0;

	std::cout << "Введите ответ:" << std::endl;
	std::cout << a << ' ' << (v == 0 ? "+" : (v == 1 ? "-" : "*"))  << ' ' << b << " = ";
	std::cin >> c;

	_asm {
		mov ax, a
		mov bx, b
		cmp v, 0
		je sum
		cmp v, 1
		je subtract
		cmp v, 2
		je multiply
	sum :
		add ax, bx
		cmp ax, c
		je right
		jne mistake
	subtract :
		sub ax, bx
		cmp ax, c
		je right
		jne mistake
	multiply :
		imul ax, bx
		cmp ax, c
		je right
		jne mistake
	right :
		mov result, 1
	mistake :
	}

	std::cout << (result == 0 ? "Ответ неверный. Капча не пройдена." : "Ответ верный. Капча пройдена.") << std::endl;
}
