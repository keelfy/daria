#include <iostream>

using namespace std;

int main()
{
    int sum = 0;
    int a, b;

    while (true)
    {
        cout << "Enter value A => ";
        cin >> a;
        b = 2;
        if (a == 0)
            return -1;

        _asm
        {
            mov EAX, a
            mov EBX, b
            div EBX
            mov sum, EDX
        }

        if (sum == 0) {
            cout << "True" << endl;
        } else {
            cout << "False" << endl;
        }
    }

    system("pause");
}
