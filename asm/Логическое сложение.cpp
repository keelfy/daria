#include <iostream>

using namespace std;

int main() {

    setlocale(LC_ALL, "rus");
    short a, b, c;
    cout << "Введите a и b => ";
    cin >> a;
    cin >> b;

    _asm {
        mov ax, a
        mov bx, b
        or ax, bx
        mov c, ax
    }

    cout << "c = " << c << endl;

    system("pause");
    return 0;
}
