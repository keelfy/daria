/**
Дано количество мест в вагоне.
Требуется программа, которая предлагает купить эти места.
Должны записываться номер паспорта, ФИО, пол и возраст.
Если билет детский, то у него другая стоимость + к нему должен быть куплен хотя бы один билет для взрослого.
Если билет льготный, то его стоимость тоже отличается.
И должно выводиться количество оставшихся мест.
Если паспортные данные или что-то еще совпадают, то так и должно писаться.
*/

#include <iostream>
#include <string.h>
#include <iomanip>

using namespace std;

enum TicketType {
    NORMAL, CHILDREN, PREFERENTIAL, EMPTY
};

enum PlaceType {
    LOWER, UPPER, SIDED_LOWER, SIDED_UPPER
};

struct Ticket {
    // System data
    TicketType type = EMPTY;
    unsigned int number;
    PlaceType placeType;
    unsigned int place;

    // Personal data
    unsigned int passport;
    string fullname;
    unsigned int gender;
    unsigned int age;
    unsigned int elder;
};

static float price;
static string current_date;
static unsigned int size;
static Ticket* tickets;

PlaceType getPlaceTypeByNumber(unsigned int placeNumber) {
    unsigned int roomAmount = size / 6;
    unsigned int kAmount = roomAmount * 4;

    if (placeNumber < kAmount && placeNumber % 2 == 0) {
        return LOWER;
    } else if (placeNumber < kAmount && placeNumber % 2 == 1) {
        return UPPER;
    } else if (placeNumber >= kAmount && placeNumber % 2 == 0) {
        return SIDED_LOWER;
    } else if (placeNumber >= kAmount && placeNumber % 2 == 1) {
        return SIDED_UPPER;
    }
    return LOWER; // Никогда не будет вызвано
};

unsigned int findLastFreePlace() {
    for (unsigned int i = 0; i < size; i++) {
        if (tickets[i].type == EMPTY)
            return i;
    }
    return size + 1;
};

unsigned int findFreePlaceByType(PlaceType type) {
    for (unsigned int i = 0; i < size; i++) {
        if (tickets[i].type == EMPTY && getPlaceTypeByNumber(i) == type) {
            return i;
        }
    }
    return size + 1;
};

string getPlaceName(PlaceType type) {
    string place = "";
    switch (type) {
    case LOWER:
        place = "Н-";
        break;
    case UPPER:
        place = "В-";
        break;
    case SIDED_LOWER:
        place = "НБ-";
        break;
    case SIDED_UPPER:
        place = "ВБ-";
        break;
    }
    return place;
};


Ticket fillTicket(TicketType type) {
    unsigned int action;
    Ticket ticket;

    switch (type) {
    case NORMAL:
        cout << "Цена за билет: " << price << endl;
        break;
    case CHILDREN:
        cout << "Цена за билет: " << price * 0.3F << endl;
        break;
    case PREFERENTIAL:
        cout << "Цена за билет: " << price * 0.5F << endl;
        break;
    default:
        return Ticket();
    }
    ticket.type = type;

    cout << "Хотите выбрать место посадки?\n1. Да\n2. Автоматический выбор\n=> ";
    cin >> action;

    if (action == 1) {
        while (true) {
            cout << "Какое место предпочитаете?\n1. Нижнее\n2. Верхнее\n3. Боковое нижнее\n4. Боковое верхнее\n=> ";
            int placeTypeId;
            cin >> placeTypeId;

            PlaceType placeType = PlaceType(placeTypeId - 1);
            unsigned int place = findFreePlaceByType(placeType);
            if (place == size + 1) {
                cout << "- Свободных мест в данной категории не найдено!\n1. Выбрать другое\n2. Продолжить без выбора\n=> ";
                cin >> action;
                if (action == 1)
                    continue;
                else
                    break;
            }
            ticket.placeType = placeType;
            ticket.place = place;
            cout << "Свободное место " << getPlaceName(ticket.placeType) << ticket.place + 1 << " найдено!" << endl;
            break;
        }
    } else {
        ticket.place = findLastFreePlace();
        ticket.placeType = getPlaceTypeByNumber(ticket.place);
    }

    if (ticket.type != CHILDREN) {
        cout << "Номер паспорта владельца: ";
        cin >> ticket.passport;
    }

    cout << "ФИО владельца: " << flush;
    cin.ignore();
    getline(cin, ticket.fullname);
    cout << "Пол владельца (0 - Муж., 1 - Жен.): ";
    cin >> ticket.gender;
    if (ticket.gender != 0 && ticket.gender != 1) {
        cout << "Некорректные данные пола." << endl;
        ticket.gender = 2;
    }
    cout << "Возраст владельца: ";
    cin >> ticket.age;
    return ticket;
};

unsigned int buyTicket(unsigned int index) {
    unsigned int action;
    cout << "Какой тип билета вы хотите купить?\n1. Обычный\n2. Льготный\n3. Отменить покупку\n=> ";
    cin >> action;

    TicketType type;
    switch (action) {
    case 1:
        type = NORMAL;
        break;
    case 2:
        type = PREFERENTIAL;
        break;
    default:
        return index;
    }

    Ticket ticket = fillTicket(type);
    ticket.number = index++;
    tickets[ticket.place] = ticket;
    cout << "Билет куплен, его номер - " << ticket.number << endl;

    if (type != CHILDREN && index < size) {
        while (true) {
            cout << "Хотите приобрести билет для ребенка?\n1. Да\n2. Нет\n=>";
            cin >> action;

            if (action != 1)
                break;

            Ticket ticketChild = fillTicket(CHILDREN);
            ticketChild.elder = ticket.passport;
            ticketChild.number = index++;
            tickets[ticketChild.place] = ticketChild;
            cout << "Билет для ребенка куплен и прикреплен к вам, номер билета - " << ticketChild.number << endl;
        }
    }
    return index;
};

void printTicket(unsigned int number) {
    Ticket ticket = tickets[number];
    unsigned int i;
    unsigned int width = ticket.fullname.length() + 6 + 20;

    for (i = 0; i < width; i++)
        cout << '-';
    cout << endl;

    string s = "| Билет №";
    cout << s << setw(5) << ticket.number << endl;

    s = "| Место №";
    cout << s << setw(5) << getPlaceName(ticket.placeType) << ticket.place + 1 << endl;

    s = "| ";
    cout << s << setw(ticket.fullname.length() + 3) << ticket.fullname << setw(10) << ticket.passport << endl;

    for (i = 0; i < width; i++)
        cout << '-';
    cout << endl;
};

void printAllTickets() {
    cout << setw(5) << "#" << setw(50) << "FIO" << setw(10) << "Passport" << setw(5) << "L" << setw(5) << "D" << setw(10) << "Seat" << endl; // Англ. заголовки, ибо если ставить русский - вся таблица съедит
    for (unsigned int i = 0; i < size; i++) {
        if (tickets[i].type == EMPTY)
            continue;
        string place = getPlaceName(tickets[i].placeType);
        cout << setw(5) << tickets[i].number << setw(50) << tickets[i].fullname << setw(10);
        if (tickets[i].type == CHILDREN)
            cout << '-';
        else
            cout << tickets[i].passport;
        cout << setw(5) << (tickets[i].type == PREFERENTIAL ? '+' : '-') << setw(5) << (tickets[i].type == CHILDREN ? '+' : '-') << setw(10) << place << tickets[i].place + 1 << endl;
    }
};

int main()
{
    setlocale(LC_ALL, "rus");
    unsigned int action, number, index = 0;
    cout << "Цена за обычный билет = ";
    cin >> price;
    cout << "Количество мест в вагоне (6-и-кратное число) = ";
    cin >> size;
    if (size % 6 != 0) {
        cout << "Количество мест должно быть 6-и-кратным!" << endl;
        return -1;
    }
    cout << "Дата поездки (ДД:ММ:ГГ) => " << flush;
    cin.ignore();
    getline(cin, current_date);
    tickets = new Ticket[size];

    while (true) {
        cout << "--- Меню ---\n1. Купить билет\n2. Напечатать билет\n3. Напечатать все купленные билеты\n4. Выход из программы\n=> ";
        cin >> action;

        switch (action) {
        case 1:
            cout << "Осталось " << size - index << " билетов." << endl;
            index = buyTicket(index);
            break;
        case 2:
            cout << "Номер билета для вывода: ";
            cin >> number;
            printTicket(number);
            break;
        case 3:
            printAllTickets();
            break;
        default:
            return 0;
        }
    }
}