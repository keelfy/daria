#include <iostream>
#include <stack>

using namespace std;

struct Subject {
    stack<unsigned int> rates;
    string name;
};

struct Diary {
    stack<Subject> subjects;
};

Diary inputDiary() {
    Diary diary = Diary();
    unsigned int rate;

    while (true) {
        Subject subject;

        cout << "Название предмета (end - прекратит ввод дневника) => " << flush;
        if (diary.subjects.size() != 0)
            cin.ignore();
        getline(cin, subject.name);
        if (subject.name == "end")
            break;

        cout << "Введите оценки по предмету (для завершения введите 0) => ";
        while (true) {
            cin >> rate;
            if (rate == 0)
                break;
            if (rate > 5) {
                cout << "Ошибка! Неккоректная оценка." << endl;
                continue;
            }
            subject.rates.push(rate);
        }

        diary.subjects.push(subject);
    }
    return diary;
};

void logDiary(Diary diary) {
    unsigned int j, ratesSize = 0;
    unsigned int size = diary.subjects.size();
    unsigned int i = size - 1;
    string* subjects = new string[size];
    unsigned int** rates = new unsigned int*[size];
    unsigned int* sizes = new unsigned int[size];

    while (!diary.subjects.empty()) {
        ratesSize = diary.subjects.top().rates.size();
        subjects[i] = diary.subjects.top().name;
        rates[i] = new unsigned int[ratesSize];
        sizes[i] = ratesSize;
        j = ratesSize - 1;
        while (!diary.subjects.top().rates.empty()) {
            rates[i][j] = diary.subjects.top().rates.top();
            diary.subjects.top().rates.pop();
            --j;
        }
        diary.subjects.pop();
        --i;
    }

    cout << "--- Дневник ---" << endl;
    for (i = 0; i < size; i++) {
        cout << subjects[i] << ":";
        for (j = 0; j < sizes[i]; j++) {
            cout << ' ' << rates[i][j];
        }
        cout << endl;
    }
};

int main()
{
    Diary diary = inputDiary();
    logDiary(diary);
}
