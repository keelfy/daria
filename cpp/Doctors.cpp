#include <iostream>
#include <stack>

using namespace std;

/** Структура одного доктора со стэком его пациентов */
struct Doctor {
    string name;
    stack<unsigned int> cards;
};

/** Эта функция вводит врачей и возвращает их стэк */
stack<Doctor> inputDoctors() {
    stack<Doctor> doctors; // стэк врачей, которые будут введены

    while (true) { // бесконечный цикл ввода врачей
        Doctor doctor; // вводимый врач (буффер)
        unsigned int card; // вводимый номер карточки (буффер)
        cout << "Введите имя врача (end - прекратит ввод данных) => " << flush;
        if (doctors.size() != 0) // эта проверка нужна для того, чтобы корректно вводить имена врачей
            cin.ignore(); // там есть баг, который ломает весь ввод, если перед getline() не написать cin.ignore()
        getline(cin, doctor.name); // вводит имя врача
        if (doctor.name == "end") // проверяем, если введенное имя == end, значит пользователь захотель прекратить ввод
            break; // обрываем цикл на этом моменте

        cout << "Введите номера карточек пациентов (0 - завершит ввод пациентов) => ";
        while (true) { // бесконечный цикл ввода карточек
            cin >> card; // вводим карточку
            if (card == 0) // если номер карточки == 0, то обрываем цикл на этом моменте
                break;
            doctor.cards.push(card); // если всё ок, то добавляем введенный номер карточки в стэк карточек текущего врача
        }
        doctors.push(doctor); // если ваще всё ок, то добавляем самого врача в стэк всех врачей
    }
    return doctors; // возвращаем стэк всех врачей, после завершения ввода
};

/** 
 * Эта функция вывод информацию о каком-то конкретном враче
 * В параметры функции передается стэк всех врачей и имя врача, о котором нужно всё вывести
 */
void doctorInfo(stack<Doctor> doctors, string name) {
    unsigned int i, j, cardsSize = 0; // cardsSize - начальный размер стэка карточек
    unsigned int size = doctors.size(); // начальный размер стэка врачей
    unsigned int* inverted = NULL; // инвертированный массив карточек

    for (i = 0; i < size; i++) { // цикл по всем врачам
        if (doctors.top().name == name) { // если у текущего врача имя такое же, какое нам нужно
            cardsSize = doctors.top().cards.size(); // размер стэка карточек приравниваем к размеру стэка карточек этого врача
            inverted = new unsigned int[cardsSize]; // инициализируем инвертированный массив карточек
            for (j = 0; j < cardsSize; j++) { // цикл по карточкам
                inverted[cardsSize - 1 - j] = doctors.top().cards.top(); // приравниваем элемент массив верхней карточки
                doctors.top().cards.pop(); // убираем верхнюю карточку
            }
            break; // обрываем цикл на этом моменте
        } else {
            doctors.pop(); // если врач не тот, то убираем верхнего врача
        }
    }

    if (inverted == NULL) {
        cout << "Врач с таким именем не найден!";
    } else {
        cout << "Номера карточек пациентов врача " << name << ":";
        for (i = 0; i < cardsSize; i++) {
            cout << ' ' << inverted[i];
        }
        cout << endl;
    }
};

int main()
{
	/** Стэк всех врачей */
    stack<Doctor> doctors = inputDoctors();

    /** Имя врача, о котором нужна информация */
    string name;
    /** Номер действия в меню */
    unsigned int action;

    while (true) {
    	cout << "--- Меню ---\n1. Вывести информацию о враче\n2. Завершить работу программы\n=> "
    	cin >> action;
    	/** Если выбранное действие не является пунктом с выводом информации - то обрываем цикл */
    	if (action != 1)
    		break; // break - стопает цикл на этом моменте
	    cout << "Введите имя врача, пациентов которого вы хотите увидеть => " << flush;
	    cin.ignore()
	    getline(cin, name);
	 	/** Вывод информации о введенном враче */
	    doctorInfo(doctors, name);
	}
}
