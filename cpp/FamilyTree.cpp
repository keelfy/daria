#include <iostream>

using namespace std;

unsigned int Count = 0;

typedef struct FamilyTree {
    FamilyTree* f;
    FamilyTree* m;
    char* fio = new char[30];
} FamilyT;

FamilyTree* MakeTree(unsigned int n, const char* oldfio, char per) {
    FamilyTree* p;
    p = new FamilyTree;
    Count++;
    if (n >= 1) {
        switch (per) {
        case 'f': cout << "Введите ФИО отца " << oldfio << " :" << endl;
            cin.getline(p->fio, 30);
            oldfio = p->fio;
            break;
        case 'm': cout << "Введите ФИО матери " << oldfio << " :" << endl;
            cin.getline(p->fio, 30);
            oldfio = p->fio;
            break;
        default: cout << "Введите ваше ФИО: " << endl;
            cin.getline(p->fio, 30);
            oldfio = p->fio;
            break;
        }
        p->f = MakeTree(n - 1, oldfio, 'f');
        p->m = MakeTree(n - 1, oldfio, 'm');
    }
    return p;
}

void print_tree(unsigned int n, FamilyTree* p)
{
    char t[n];
    cout << "Введите родословную, начиная с вас (т.е. я)" << endl;

    for (unsigned i = 0; i < n; i++) {
        cin >> t[i];
        if (t[i] == 'п') {
            p = p->f;
            cout << "--->" << p->fio;
        } else if (t[i] == 'м') {
            p = p->m;
            cout << "--->"<< p->fio;
        } else if (t[i] == 'я') {
            cout << p->fio;
        }
    }
}

int main() {
    setlocale(LC_ALL, "rus");
    FamilyTree* q;
    q = MakeTree(4, "", 'i');
    print_tree(4, q);
    cout << endl;
    system("pause");
}
