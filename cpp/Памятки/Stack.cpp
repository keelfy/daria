#include <iostream>
#include <stack>

using namespace std;

int main()
{
    // В <> указывается хранящийся в стэке тип данных
    // Это может быть любой тип, включая string, свои структуры, свои классы и т.д. 
    stack<int> stack;

    // Команда push добавляет "верхний" (последний) элемент
    stack.push(10);
    stack.push(20);
    stack.push(30);
    /* Стэк теперь имеет вид
    30
    20
    10
    */

    // Команда pop удаляет "верхний" (последний) элемент
    stack.pop();
    /* После
    20
    10
    */

    int lastElement = stack.top(); // Возвращает верхний элемент, то есть 20
    cout << "Latest stack element is " << lastElement << endl;

    bool empty = stack.empty(); // Пуст ли стэк?
    cout << "Is stack empty? " << empty << endl;

    unsigned int size = stack.size(); // Текущий размер стэка
    cout << "Current stack size is " << size << endl;

    // Команда ниже работает только на новых версиях С++ (С++11) 
    // P.S. Это значит, что её вряд ли будут требовать на учёбе, учатся на старых версиях С++
    // и возвращает элемент по индексу,
    // Например, у числа 20 индекс = 1, у числа 10 = 0 - то есть нумерация "снизу" стэка
    int element = stack.peek(index);

    return 0;
}
